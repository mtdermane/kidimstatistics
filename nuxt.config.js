import colors from 'vuetify/es5/util/colors'
import webpack from 'webpack'

export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/images/logo.png' }
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/images/logo.png'},
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [

  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
  ],
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.red,
          primary2: colors.red.darken1,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          background: colors.orange.lighten5
        },
        light: {
          primary: '#ff3333',
          primary2: '#f28a3a',
          primary3: '#FFE0B2',
          primary4: '#ffb175',
          secondary: '#f57c00',
          tertiary: '#7D3802',
          accent: '#8c9eff',
          error: '#b71c1c',
          dashboard: '#8E24AA',
          menu: '#AD1457',
          profile: '#303F9F',
          history: '#F57C00',
          settings: '#388E3C',
          logout: '#6D4C41',
        },
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    plugins: [
      new webpack.ProvidePlugin({
        '_': 'lodash'
      })
    ],
    babel: {
      plugins: ['transform-commonjs-es2015-modules']
    },
   transpile: ['feathers-vuex'],
    extend (config, ctx) {
    }
  }
}
