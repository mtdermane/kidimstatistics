import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import moment from 'moment'

Vue.use(VueAxios, axios)
Vue.use(require('moment'))
Vue.use(require('vue-moment'))

/** Check moment to translate the date  
 * https://momentjs.com/docs/#/i18n/listing-months-weekdays/
*/

const getPeriodData = (url, period, username) => {
    //get the last two days two days
    let endDate = moment().add(2, 'days').format('YYYY-MM-DD')
    let startDate = moment().subtract(period, 'days').format('YYYY-MM-DD')

    //connect to server
    const params = new URLSearchParams()
    params.append('startDate', startDate)
    params.append('endDate', endDate)
    params.append('username', username)
    return axios({
        method: 'post',
        url: url,
        data: params
    })
}

const getTheMenu = (placeId, language, table) => {
    const params = new URLSearchParams()
    params.append('placeId', placeId)
    params.append('language', language)
    params.append('table', table)
    return axios({
        method: 'post',
        url: "https://www.hkmanager.com/orderapp/kidim/webServices/customer/m_get_menu.php",
        data: params
    })
}

export {
    getPeriodData,
    getTheMenu
}