import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class Category extends BaseModel {
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Category";
  // Define default properties here
  static instanceDefaults() {
    return {
      placeId: "",
      name: "",
      language: "en",
    };
  }
}
const servicePath = "category";
const servicePlugin = makeServicePlugin({
  Model: Category,
  service: feathersClient.service(servicePath),
  servicePath
});

export default servicePlugin;
