import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class Order extends BaseModel {
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Order";
  // Define default properties here
  static instanceDefaults() {
    return {
      table: 0,
      placeId: null,
      order: [],
      seen: false,
    };
  }
}
const servicePath = "orders";
const servicePlugin = makeServicePlugin({
  Model: Order,
  service: feathersClient.service(servicePath),
  servicePath
});

export default servicePlugin;
