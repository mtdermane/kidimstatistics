import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class Menu extends BaseModel {
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Menu";
  // Define default properties here
  static instanceDefaults() {
    return {
      placeId: null,
      language: "en",
      category: "",
      name: "",
      price: "",
      description: "",
      option: {}
    };
  }
}
const servicePath = "menu";
const servicePlugin = makeServicePlugin({
  Model: Menu,
  service: feathersClient.service(servicePath),
  servicePath
});

export default servicePlugin;
