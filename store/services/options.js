import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class Option extends BaseModel {
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Option";
  // Define default properties here
  static instanceDefaults() {
    return {
      placeId: null,
      period: 30,
      dashboard: [1,2,3,4,5],
    };
  }
}
const servicePath = "options";
const servicePlugin = makeServicePlugin({
  Model: Option,
  service: feathersClient.service(servicePath),
  servicePath
});

export default servicePlugin;
