import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class Place extends BaseModel {
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Place";
  // Define default properties here
  static instanceDefaults() {
    return {
      name: "",
      currency: "",
      address: "",
      phone: "",
      email: "",
      tables: [],
      ok: "",
      options: {}
    };
  }
}
const servicePath = "place";
const servicePlugin = makeServicePlugin({
  Model: Place,
  service: feathersClient.service(servicePath),
  servicePath
});

export default servicePlugin;
