FROM node:10.16.3
# install simple http server for serving static content
RUN npm install -g http-server
WORKDIR .
# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./
# install project dependencies
RUN npm install
# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .
# build app for production with minification
RUN npm run build
EXPOSE 8080
CMD [ "http-server", "dist" ]
EOF
# now let's use docker to build a docker image
docker build -t vue-google-cloud/vue-app .
# let's run it to verify it all works.  
docker run -d -p 8080:8080 --rm vue-google-cloud/vue-app
# check to see that it's working
open localhost:8080
# get the running docker containers
docker ps
# stop the container.  the id will look similar to 386ab1e23ecd
docker stop matradesoft